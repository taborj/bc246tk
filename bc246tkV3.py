#!/usr/bin/env python

# (C) 2015 Jon Tabor
# taborj@jontabor.tk
# http://jontabor.tk/wordpress

#################################################
# BC246T display program                        #
# Displays the status screen from the BC246T    #
# and also allows you to control as if you were #
# using the keypad.                             #
#                                               #
# Usage: bc246tkV3.py /dev/ttyUSB0              #
#                                               #
#################################################

#################################################
# Version 2 additions:                          #
#   * Bug fixes                                 #
#   * Logging                                   #
#   * Power off button                          #
#   * More commenting of code                   #
#################################################
# Version 3 additions:                          #
#   * Bug fixes                                 #
#   * Keypad                                    #
#   * Layout change, including                  #
#       the inclusion of SYS/GRP icons          #
#################################################

#############################################
#  Special thanks to Ryon Sherman           #
#  for his work on bc246t.py, which         #
#  I used for reference on the commands     #
#  for the BC246t scanner, and obviously    #
#  not for my programming practices.        #
#   https://github.com/ryonsherman/bc246t   #
#############################################

import sys
import serial
import signal
from Tkinter import *
import time
import notify2


# Open the serial port with a .1 second timeout
def ser_init():
    serport = sys.argv[1] 
    bc246t=serial.Serial(serport, 57600, timeout=.1)
    return bc246t

ser = ser_init()

# Initialize the Tk window
root = Tk()
root.resizable(0,0)
root.title("Uniden BC246T Display")
img = PhotoImage(file='bc246tk.png')
root.tk.call('wm', 'iconphoto', root._w, img)


# Global vars
line1 = StringVar()
line2 = StringVar()
line2b = StringVar()
lineSys = StringVar()
lineGrp = StringVar()
prev_output = StringVar()
held = False
logging_on = False
logged = False
funcHeld = False
notify_on = False
notified = False
message = ''
notice = notify2.Notification('BC246T', message)

# Ask the scanner for it's status
def getstatus():
    ser.writelines('STS\r')
    line=ser.readline()
    status_list = line.split(',')
    if len(status_list) < 2:
        ser.writelines('STS\r')
        line=ser.readline()
        status_list = line.split(',')
    return status_list
    
# Get the TG ID
def getTG():
    ser.writelines('GID\r')
    gid=ser.readline()
    gid_list = gid.split(',')
    return gid_list

# Functions used by buttons 
# Hold
def hold():
    global held
    ser.writelines('KEY,H,P\r')
    held = not held
    #if bHold.cget('relief') == SUNKEN:
    if (held == True):
        bHold.configure(relief=SUNKEN)
    #   buHold.configure(relief=SUNKEN)
    else:
        bHold.configure(relief=RAISED)
    #   buHold.configure(relief=SUNKEN)
        
    stat_list = getstatus()
    if (stat_list[1][0:2] == "OK"):
        stat_list = ['','','','']
    return

# Generic Push A Key function
def pushKey(key,duration):
    global funcHeld
    global held
    if (key == 'F'):
        if (funcHeld == False):
            duration = 'H'
            funcHeld = not funcHeld
        else:
            duration = 'R'
            funcHeld = not funcHeld

    ser.writelines('KEY,%s,%s\r' % (key, duration))

    if (funcHeld == True) and (key != 'F'):
        ser.writelines('KEY,F,R')
        funcHeld = not funcHeld

    if (held == True) and (key == 'S'):
        bHold.configure(relief=RAISED)
        held = not held
    return


# Enable logging
def enable_log():
    systemdate = time.strftime("%Y") + "-" + time.strftime("%m") + "-" + time.strftime("%d")
    systemtime = time.strftime("%H") + ":" + time.strftime("%M") + ":" + time.strftime("%S")
    global logging_on
    f = open('bc246t_log.csv','a')
    if logging_on == True:
        logging_on = False
        out = ('*** Logging disabled [%s - %s] ***\n' % (systemdate, systemtime))
    else:
        logging_on = True
        out = ('*** Logging enabled [%s - %s] ***\nDate,Time,Group,Name,TGID\n' % (systemdate, systemtime))
    f.write(out)
    return
    
# Log to file is CSV format
def logging(group,talkgroup, tg_id):
    systemdate = time.strftime("%Y") + "-" + time.strftime("%m") + "-" + time.strftime("%d")
    systemtime = time.strftime("%H") + ":" + time.strftime("%M") + ":" + time.strftime("%S")
    f = open('bc246t_log.csv','a')
    out = ('%s,%s,%s,%s,%s\n' % (systemdate, systemtime,group,talkgroup,tg_id))
    f.write(out)
    f.close
    return
    
# Enable python-notify notifications
def enable_notify():
    global notify_on
    notify_on = not notify_on
    return

# Turn the scanner off  
def poweroff():
    ser.writelines('POF\r')
    line1.set('Powered off')
    return

# Reconnect to the scanner  
def reconnect():
    #ser.close()
    time.sleep(2)
    ser = ser_init()
    self.id=self.after(100,self.callme)
    return


# Buttons and Labels 
# Drop-down
options = Menubutton(root, text="Opt", relief = RAISED)
options.menu = Menu(options, tearoff = 0)
options["menu"] = options.menu
options.menu.add_checkbutton(label = 'Enable Notify', command = enable_notify)
options.menu.add_checkbutton(label = 'Logging', command = enable_log)
options.menu.add_command(label = 'Power off', command = poweroff)
options.menu.add_command(label = 'Reconnect', command = reconnect)
options.config(bg='lightgrey', bd=1, activebackground = 'grey', font='Helvetica 7', width=4)
options.grid(row=2, column=0)

# Main screen turn on
l = Label(root, width = 16, font='Courier 10 bold', textvariable = line1, bg = 'lightgrey')
l.grid(row=0, column=1)

l2 = Label(root, width = 16, font='Courier 10 bold', textvariable = line2, bg = 'lightgrey')
l2.grid(row=1, column=1)

l3 = Label(root, width = 12, font='Courier 10 bold', textvariable = line2b, bg = 'lightgrey')
l3.grid(row=1,column=2, columnspan=2, sticky='w')

lsys = Label(root,width = 16, font='Courier 7 bold', textvariable = lineSys, bg = 'lightgrey', anchor='w')
lsys.grid(row=2, column=1, columnspan = 2, sticky='w')

lgrp = Label(root,width = 16, font='Courier 7 bold', textvariable = lineGrp, bg = 'lightgrey')
lgrp.grid(row=2, column=2, columnspan=2)

# Exit button
bExit = Button(root, text="Exit", width = 1, command=root.destroy, font='Helvetica 7')
bExit.grid(row=1, column=0)

# Hold button
bHold = Button(root, text="Hold", command=hold, height = 1, width = 4, font='Helvetica 7')
bHold.grid(row=0, column=3)

# Scan button
bScan = Button(root, text="Scan", command=lambda: pushKey('S','P'), height = 1, width = 4, font='Helvetica 7')
bScan.grid(row=0, column =2)

    

class bc246(Frame):
    
    def __init__(self, *args, **kwargs):

        Frame.__init__(self, *args, **kwargs)
        self.x=0
        self.id=self.after(100,self.callme)
        # Keypad button
        bKey = Button(root, text="Key", command=self.create_window, height = 1, width = 1, font='Helvetica 7')
        bKey.grid(row=0, column=0)


    def callme(self):
        
        global logging_on
        global logged
        global notified
        global notice
        notify2.init("BC246T")
        stat_list = getstatus()
        
        if (stat_list[1][0:2] == "OK") and (held == '1'):
            stat_list = ['','','','']
        if (stat_list[1][0:2] == "OK"):
            stat_list = getstatus()

        
        tg_list = getTG()
        
        if (len(tg_list) < 2) or (tg_list[1] == ''):
            tg = '     '
            tg2 = tg
            group = stat_list[1]
            name = stat_list[3]
            sys_icon = stat_list[5]
            grp_icon = stat_list[6]
        else:
            tg = tg_list[2]
            tg2 = '(' + tg + ')'
            system = tg_list[4]
            group = tg_list[5]
            name = tg_list[6].strip('\r')
            sys_icon = stat_list[5]
            grp_icon = stat_list[6]
        
        # Is the MUTE flag set?
        if len(stat_list) > 2:          
            if(stat_list[9] == '1'):
                # If so, turn off the 'backlight'
                l.configure(background = 'lightgrey')
                l2.configure(background = 'lightgrey')
                logged = False
                notified = False
                root.title("Uniden BC246T Display")
            else:
                # If the flag is not set, turn on the 'backlight'
                # and log to a file, if logging is turned on.   
                l.configure(background = 'orange')
                l2.configure(background = 'orange')
                    
                if (logging_on == True) and (logged == False):
                    logging(group,name, tg)
                    logged = True
                if (notify_on == True) and (notified == False):
                    notice.close()
                    message = group + '\r' + name + ' ' + tg2
                    notice = notify2.Notification('BC246T', message)
                    #notice.add_action("hold", "Hold", hold)
                    notice.show()
                    #root.title('%s %s' % (name, tg2))
                    notified = True
        
        # Build out the SYS and GRP icon lines          
        count = 1
        sysline = ''
        for x in sys_icon[1:11]:
            if (x == '1'):
                if (count == 10):
                    sysline = sysline + '0'
                else:
                    sysline = sysline + str(count)
            else:
                sysline = sysline + ' '
            count += 1
            #sysline = sysline + ' '

        count = 1
        grpline = ''
        for x in grp_icon[1:11]:
            if (x == '1'):
                if (count == 10):
                    grpline = grpline + '0'
                else:
                    grpline = grpline + str(count)
            else:
                grpline = grpline + ' '
            count += 1


        # Set the screen output
        line1.set('%s' % group)
        line2.set('%s' % name)
        line2b.set('TG: %s' % tg)
        lineSys.set('SYS: %s' % sysline)
        lineGrp.set('GRP: %s' % grpline)
        
        root.title('%s %s' % (name, tg2))
        
        root.update_idletasks()
        self.id=self.after(100,self.callme)
        
    # Set up the keypad window.  Lots of buttons.
    def create_window(self):
        global held
        global buHeld
        t = Toplevel(self)
        buHold = Button(t, text="Hold", command=hold, height = 1, width = 1)
        buHold.grid(row=0, column=0)
        b1 = Button(t, text="1", command=lambda: pushKey(1,'P'), height = 1, width = 1).grid(row=0, column=1)
        b2 = Button(t, text="2", command=lambda: pushKey(2,'P'), height = 1, width = 1).grid(row=0, column=2)
        b3 = Button(t, text="3", command=lambda: pushKey(3,'P'), height = 1, width = 1).grid(row=0, column=3)
        buScan = Button(t, text="Scan", command=lambda: pushKey('S','P'), height = 1, width = 1).grid(row=1, column=0)
        b4 = Button(t, text="4", command=lambda: pushKey(4,'P'), height = 1, width = 1).grid(row=1, column=1)
        b5 = Button(t, text="5", command=lambda: pushKey(5,'P'), height = 1, width = 1).grid(row=1, column=2)
        b6 = Button(t, text="6", command=lambda: pushKey(6,'P'), height = 1, width = 1).grid(row=1, column=3)
        buLO = Button(t, text="L/O", command=lambda: pushKey('L','P'), height = 1, width = 1).grid(row=2, column=0)
        b7 = Button(t, text="7", command=lambda: pushKey(7,'P'), height = 1, width = 1).grid(row=2, column=1)
        b8 = Button(t, text="8", command=lambda: pushKey(8,'P'), height = 1, width = 1).grid(row=2, column=2)
        b9 = Button(t, text="9", command=lambda: pushKey(9,'P'), height = 1, width = 1).grid(row=2, column=3)
        buLight = Button(t, text="Lt", command=lambda: pushKey('!','P'), height = 1, width = 1).grid(row=3, column=0)
        bNo = Button(t, text="No", command=lambda: pushKey('.','P'), height = 1, width = 1).grid(row=3, column=1)
        b0 = Button(t, text="0", command=lambda: pushKey(0,'P'), height = 1, width = 1).grid(row=3, column=2)
        bE = Button(t, text="E", command=lambda: pushKey('E','P'), height = 1, width = 1).grid(row=3, column=3)
        buFunc = Button(t, text="Func", command=lambda: pushKey('F','P'), height = 1, width = 1)
        buFunc.grid(row=4, column=0)
        bLt = Button(t, text="<", command=lambda: pushKey('<','P'), height = 1, width = 1).grid(row=4, column=1)
        bPush = Button(t, text="^", command=lambda: pushKey('^','P'), height = 1, width = 1).grid(row=4, column=2)
        bRt = Button(t, text=">", command=lambda: pushKey('>','P'), height = 1, width = 1).grid(row=4, column=3)
        bMenu = Button(t, text="Menu", command=lambda: pushKey('M','P'), height = 1, width = 8).grid(row=5, column=0, columnspan=4)

        if (held == True):
            buHold.configure(relief=SUNKEN)
        else:
            buHold.configure(relief=RAISED)
        if (funcHeld == True):
            buFunc.configure(relief=SUNKEN)
        else:
            buFunc.configure(relief=RAISED)

        #b3.pack(side = "top")
        #b2.pack(side = "right")
        #b1.pack(side = "top")
        #buHold.pack(side = "top")
        #b4.pack(side = "bottom")
        #b5.pack(side = "bottom")
        #b6.pack(side = "bottom")
        #buScan.pack(side = "bottom")

#root=bc246()
#root.mainloop()

if __name__ == "__main__":
    #root = tk.Tk()
    main = bc246(root)
    #main.pack(side="top", fill="both", expand=True)
    main.grid()
    root.mainloop()
