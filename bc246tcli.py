# Command-line interface for the Uniden BC246T scanner
#
# Usage: bc246tcli.py /dev/ttyUSB0
#

#!/usr/bin/env python
import sys
import serial
import signal

# Open the serial port with a .3 second timeout
ser=serial.Serial(sys.argv[1], 57600, timeout=.3)

# Set up the interrupt handler -- we'll run the while loop below until
# the user presses Ctrl+C
def signal_handler(signal, frame):
    print 'Exiting...'
    ser.close()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)
print 'Press Ctrl+C to exit'
while True:
  ser.writelines('STS\r')
  line=ser.readline()
  group=line[4:19]
  status=line[38:53]
  sys.stdout.write('[%s] ' % group)
  sys.stdout.write('{%s}\r' % status)
  sys.stdout.flush()
